<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rate
 *
 * @ORM\Table(name="rate")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RateRepository")
 */
class Rate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="usd_buy", type="string")
     */
    private $usdBuy;

    /**
     * @var string
     *
     * @ORM\Column(name="usd_sell", type="string")
     */
    private $usdSell;

    /**
     * @var string
     *
     * @ORM\Column(name="eur_buy", type="string")
     */
    private $eurBuy;

    /**
     * @var string
     *
     * @ORM\Column(name="eur_sell", type="string")
     */
    private $eurSell;

    /**
     * @var string
     *
     * @ORM\Column(name="rub_buy", type="string")
     */
    private $rubBuy;

    /**
     * @var string
     *
     * @ORM\Column(name="rub_sell", type="string")
     */
    private $rubSell;

    /**
     * @var string
     *
     * @ORM\Column(name="kzt_buy", type="string")
     */
    private $kztBuy;

    /**
     * @var string
     *
     * @ORM\Column(name="kzt_sell", type="string")
     */
    private $kztSell;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetime")
     */
    private $datetime;


 
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usdBuy
     *
     * @param string $usdBuy
     *
     * @return Rate
     */
    public function setUsdBuy($usdBuy)
    {
        $this->usdBuy = $usdBuy;

        return $this;
    }

    /**
     * Get usdBuy
     *
     * @return string
     */
    public function getUsdBuy()
    {
        return $this->usdBuy;
    }

    /**
     * Set usdSell
     *
     * @param string $usdSell
     *
     * @return Rate
     */
    public function setUsdSell($usdSell)
    {
        $this->usdSell = $usdSell;

        return $this;
    }

    /**
     * Get usdSell
     *
     * @return string
     */
    public function getUsdSell()
    {
        return $this->usdSell;
    }

    /**
     * Set eurBuy
     *
     * @param string $eurBuy
     *
     * @return Rate
     */
    public function setEurBuy($eurBuy)
    {
        $this->eurBuy = $eurBuy;

        return $this;
    }

    /**
     * Get eurBuy
     *
     * @return string
     */
    public function getEurBuy()
    {
        return $this->eurBuy;
    }

    /**
     * Set eurSell
     *
     * @param string $eurSell
     *
     * @return Rate
     */
    public function setEurSell($eurSell)
    {
        $this->eurSell = $eurSell;

        return $this;
    }

    /**
     * Get eurSell
     *
     * @return string
     */
    public function getEurSell()
    {
        return $this->eurSell;
    }

    /**
     * Set rubBuy
     *
     * @param string $rubBuy
     *
     * @return Rate
     */
    public function setRubBuy($rubBuy)
    {
        $this->rubBuy = $rubBuy;

        return $this;
    }

    /**
     * Get rubBuy
     *
     * @return string
     */
    public function getRubBuy()
    {
        return $this->rubBuy;
    }

    /**
     * Set rubSell
     *
     * @param string $rubSell
     *
     * @return Rate
     */
    public function setRubSell($rubSell)
    {
        $this->rubSell = $rubSell;

        return $this;
    }

    /**
     * Get rubSell
     *
     * @return string
     */
    public function getRubSell()
    {
        return $this->rubSell;
    }

    /**
     * Set kztBuy
     *
     * @param string $kztBuy
     *
     * @return Rate
     */
    public function setKztBuy($kztBuy)
    {
        $this->kztBuy = $kztBuy;

        return $this;
    }

    /**
     * Get kztBuy
     *
     * @return string
     */
    public function getKztBuy()
    {
        return $this->kztBuy;
    }

    /**
     * Set kztSell
     *
     * @param string $kztSell
     *
     * @return Rate
     */
    public function setKztSell($kztSell)
    {
        $this->kztSell = $kztSell;

        return $this;
    }

    /**
     * Get kztSell
     *
     * @return string
     */
    public function getKztSell()
    {
        return $this->kztSell;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     *
     * @return Rate
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }
}
