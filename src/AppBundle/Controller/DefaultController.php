<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Rate;
use phpQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    /**
     * @Route("/parsing")
     * @Method("POST")
     */
    public function parsingAction()
    {
        $currency = ['USD', 'EUR', 'RUB', 'KZT'];
        $data = [];
        $parse_result = $this->parseSite();

        $k = -2;
        for ($i = 0; $i < count($currency); $i++) {
            $data[$i]['name'] = $currency[$i];
            $data[$i]['buy'] = $parse_result[$k+=2];
            $data[$i]['sell'] = $parse_result[$k+1];
        }

        $this->saveRates($data);

        return new JsonResponse($data);
    }

    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $rates = $this->getDoctrine()->getRepository('AppBundle:Rate')
            ->getHistoryRates(10);

        return $this->render('@App/Default/index.html.twig', [
            'rates' => $rates
        ]);
    }

    /**
     * @return array
     */
    public function parseSite(): array
    {
        $html = file_get_contents('https://valuta.kg/');

        $document = phpQuery::newDocument($html);

        $bank_info = $document->find('#js-member-bakaibank');

        $td = $bank_info->find('td.td-rate');

        $curses = $td->text();
        $curses = trim(preg_replace("/\s{2,}/", " ", $curses));

        $array = explode(" ", $curses);
        return $array;
    }

    /**
     * @param $data
     */
    public function saveRates($data): void
    {
        $rate = new Rate;

        $rate->setUsdBuy($data[0]['buy']);
        $rate->setUsdSell($data[0]['sell']);
        $rate->setEurBuy($data[1]['buy']);
        $rate->setEurSell($data[1]['sell']);
        $rate->setRubBuy($data[2]['buy']);
        $rate->setRubSell($data[2]['sell']);
        $rate->setKztBuy($data[3]['buy']);
        $rate->setKztSell($data[3]['sell']);
        $rate->setDatetime(new \DateTime());

        $em = $this->getDoctrine()->getManager();
        $em->persist($rate);
        $em->flush();
    }
}
